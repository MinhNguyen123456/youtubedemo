package m.empire.youtubedemo.common.mvp

/**
 * Created by PC on 10/6/2017.
 */
interface BasePresenter {
    fun start()
    fun stop()
}