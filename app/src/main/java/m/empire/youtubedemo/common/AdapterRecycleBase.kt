package m.empire.youtubedemo.common.mvp

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by PC on 10/6/2017.
 */
abstract class AdapterRecycleBase<T, G : RecyclerView.ViewHolder> : RecyclerView.Adapter<G>() {
    var arrayListData: ArrayList<T> = ArrayList()
    var itemClickCallBack: ItemClickCallBack<T>? = null

    override fun getItemCount(): Int {
        return arrayListData.size
    }

    fun addListData(listData: ArrayList<T>) {
        if (listData.isEmpty()) {
            arrayListData = listData
            notifyDataSetChanged()
        } else {
            var positionStart = arrayListData.size
            arrayListData.addAll(arrayListData.size, listData)
            notifyItemRangeInserted(positionStart, arrayListData.size)
        }
    }

    interface ItemClickCallBack<in T> {
        fun onItemClick(position: Int, t: T, viewItem: View)
    }
}