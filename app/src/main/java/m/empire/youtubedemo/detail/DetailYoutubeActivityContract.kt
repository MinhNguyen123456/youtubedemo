package m.empire.youtubedemo.detail

import android.content.Intent
import m.empire.youtubedemo.common.mvp.BasePresenter
import m.empire.youtubedemo.common.mvp.BaseView

/**
 * Created by PC on 10/6/2017.
 */
interface DetailYoutubeActivityContract {
    interface View : BaseView<Presenter> {
        /**
         * show play video youtube with id
         * @idVideo : idVideo String
         */
        fun showPlayVideoYoutube(idVideo: String)

        /**
         * show full screen app
         */
        fun showFullScreen()
    }

    interface Presenter : BasePresenter {
        /**
         * load data received from intent
         * @intent: intent received
         */
        fun loadData(intent: Intent)
    }
}