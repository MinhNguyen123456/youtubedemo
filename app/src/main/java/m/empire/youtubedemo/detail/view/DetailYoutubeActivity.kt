package m.empire.youtubedemo.detail.view

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.view.*
import android.widget.LinearLayout
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import m.empire.youtubedemo.R
import m.empire.youtubedemo.databinding.ActivityDetailBinding
import m.empire.youtubedemo.detail.DetailYoutubeActivityContract
import m.empire.youtubedemo.detail.DetailYoutubeActivityPresenter
import m.empire.youtubedemo.util.DEVELOPER_KEY

/**
 * Created by PC on 10/6/2017.
 */
class DetailYoutubeActivity : YouTubeBaseActivity(), DetailYoutubeActivityContract.View, YouTubePlayer.OnInitializedListener, YouTubePlayer.OnFullscreenListener {
    var player: YouTubePlayer? = null
    companion object {
        val TAG = "DetailYoutubeActivity"
        val KEY_DATA = "detail_key_data"
    }

    lateinit var binding: ActivityDetailBinding
    lateinit var presenter: DetailYoutubeActivityContract.Presenter
    var isFullScreen: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        presenter = DetailYoutubeActivityPresenter(this)
        presenter.loadData(intent)
        ViewCompat.setTransitionName(binding.contentDetail.youtubePlayerView, "target")
        binding.contentDetail.youtubePlayerView.initialize(DEVELOPER_KEY, this)
    }

    override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, player: YouTubePlayer?, wasRestored: Boolean) {
        this.player = player
        if (!wasRestored) {
            this.player?.fullscreenControlFlags = YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT
            this.player?.setOnFullscreenListener(this)
            presenter.start()
        }
    }


    override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {

    }


    override fun showFullScreen() {
        val playerParams = binding.contentDetail.youtubePlayerView.layoutParams as LinearLayout.LayoutParams
        if (isFullScreen) {
            // When in fullscreen, the visibility of all other views than the player should be set to
            // GONE and the player should be laid out across the whole screen.
            playerParams.width = LinearLayout.LayoutParams.MATCH_PARENT
            playerParams.height = LinearLayout.LayoutParams.MATCH_PARENT
        }
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            actionBar?.hide()
            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            binding.toolbar.visibility = View.GONE
            playerParams.width = 0
            playerParams.height = ViewGroup.LayoutParams.MATCH_PARENT
            playerParams.weight = 1f
            binding.contentDetail.mainContainer.orientation = LinearLayout.HORIZONTAL
        } else {
            actionBar?.show()
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            binding.toolbar.visibility = View.VISIBLE
            playerParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            playerParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            playerParams.weight = 0f
            binding.contentDetail.mainContainer.orientation = LinearLayout.VERTICAL
            //requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
        }
    }

    override fun showPlayVideoYoutube(idVideo: String) {
        player?.cueVideo(idVideo)
    }


    override fun onFullscreen(fullScreen: Boolean) {
        isFullScreen = fullScreen
        showFullScreen()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        showFullScreen()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}