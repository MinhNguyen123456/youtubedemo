package m.empire.youtubedemo.detail

import android.content.Intent
import com.google.gson.Gson
import m.empire.youtubedemo.detail.view.DetailYoutubeActivity
import m.empire.youtubedemo.model.data.YouTubeItem

/**
 * Created by PC on 10/6/2017.
 */
class DetailYoutubeActivityPresenter(var view: DetailYoutubeActivityContract.View) : DetailYoutubeActivityContract.Presenter {
    lateinit var youtubeItem: YouTubeItem


    override fun start() {
        try {
            view.showPlayVideoYoutube(youtubeItem.id.videoId)
        } catch (ex : Exception) {
            ex.printStackTrace()
        }
    }

    override fun stop() {

    }

    override fun loadData(intent: Intent) {
        if (intent.hasExtra(DetailYoutubeActivity.KEY_DATA)) {
            var data = intent.extras.getString(DetailYoutubeActivity.KEY_DATA)
            youtubeItem = Gson().fromJson(data, YouTubeItem::class.java)
        }
    }
}