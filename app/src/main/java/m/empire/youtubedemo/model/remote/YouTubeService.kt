package m.empire.youtubedemo.model.remote

import io.reactivex.Observable
import m.empire.youtubedemo.model.data.YouTubeSearchModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by PC on 10/6/2017.
 */
interface YouTubeService {
    @GET("v3/search")
    fun getListData( @Query("maxResults") maxResults: Int = 25, @Query("channelId") id: String, @Query("part") part: String, @Query("order") videoOrder: String,@Query("key") key: String): Observable<Response<YouTubeSearchModel>>
}