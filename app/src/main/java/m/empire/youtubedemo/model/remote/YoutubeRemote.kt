package m.empire.youtubedemo.model.remote

import android.content.Context
import io.reactivex.Observable
import m.empire.youtubedemo.model.Repository
import m.empire.youtubedemo.model.data.YouTubeSearchModel
import m.empire.youtubedemo.util.DEVELOPER_KEY
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by PC on 10/6/2017.
 */
class YoutubeRemote(context: Context) : Repository<Observable<Response<String>>> {
    /**
     * single ton crate instance of our service
     */
    companion object {
        fun create(): YouTubeService {
            var logDebug = HttpLoggingInterceptor()
            logDebug.level = HttpLoggingInterceptor.Level.BODY
            var httpClient = OkHttpClient.Builder().addInterceptor(logDebug)

            var retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .baseUrl("https://www.googleapis.com/youtube/")
                    .build()
            return retrofit.create(YouTubeService::class.java)
        }
    }

    /**
     * get list data youtube
     * return ArrayList<Observable<Response<String>>>
     */
    override fun getListData(): ArrayList<Observable<Response<String>>> {
        var listData = ArrayList<Observable<Response<String>>>()
        //create().getArrayListData(DEVELOPER_KEY, "UC_x5XG1OV2P6uZZ5FSM9Ttw", "snippet", "date")
        return listData
    }

    /**
     * get data from youtube api
     * return Observable<Response<YouTubeSearchModel>>
     */
    override fun getData(): Observable<Response<YouTubeSearchModel>> {
        return create().getListData(25,"UCa8ens-OkSYR4FegKM9NNEw", "snippet", "date", DEVELOPER_KEY)
    }
}