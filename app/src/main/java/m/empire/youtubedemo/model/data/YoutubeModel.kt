package m.empire.youtubedemo.model.data

import com.google.gson.annotations.SerializedName

/**
 * Created by PC on 10/9/2017.
 */
data class YouTubeSearchModel(@SerializedName("kind") var kind: String, @SerializedName("etag") var eTag: String, @SerializedName("nextPageToken") var nexPageToken: String, @SerializedName("regionCode") var regionCode: String, @SerializedName("pageInfo") var pageInfo: YouTubePageInfo, @SerializedName("items") var youtubeItems: ArrayList<YouTubeItem>)

data class YouTubePageInfo(@SerializedName("totalResults") var totalResult: Int, @SerializedName("resultsPerPage") var resultPerPage: Int)

data class YouTubeItem(@SerializedName("kind") var kind: String, @SerializedName("etag") var eTag: String, @SerializedName("id") var id: YouTubeItemId, @SerializedName("snippet") var snippet: YouTubeSnippet)

data class YouTubeItemId(@SerializedName("kind") var id: String, @SerializedName("videoId") var videoId: String)

data class YouTubeSnippet(@SerializedName("publishedAt") var publishedAt: String, @SerializedName("channelId") var channelId: String, @SerializedName("title") var title: String, @SerializedName("description") var description: String, @SerializedName("thumbnails") var thumbnails: YoutubeThumbs, @SerializedName("channelTitle") var channelTitle: String, @SerializedName("liveBroadcastContent") var liveBroadcastContent: String)

data class YoutubeThumbs(@SerializedName("default") var default: YoutubeItemThumb, @SerializedName("medium") var medium: YoutubeItemThumb, @SerializedName("high") var high: YoutubeItemThumb)

data class YoutubeItemThumb(@SerializedName("url") var url: String, @SerializedName("width") var width: Int, @SerializedName("height") var height: Int)