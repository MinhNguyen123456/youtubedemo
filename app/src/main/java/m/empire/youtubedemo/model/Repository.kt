package m.empire.youtubedemo.model

import io.reactivex.Observable
import m.empire.youtubedemo.model.data.YouTubeSearchModel
import retrofit2.Response

/**
 * Created by PC on 10/6/2017.
 */
interface Repository<T> {
    fun getListData(): ArrayList<T>
    fun getData(): Observable<Response<YouTubeSearchModel>>
}