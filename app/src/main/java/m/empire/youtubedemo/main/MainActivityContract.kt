package m.empire.youtubedemo.main

import m.empire.youtubedemo.common.mvp.BasePresenter
import m.empire.youtubedemo.common.mvp.BaseView
import m.empire.youtubedemo.model.data.YouTubeItem

/**
 * Created by PC on 10/6/2017.
 */
interface MainActivityContract {
    interface View : BaseView<Presenter> {
        /**
         * start show progress loading
         */
        fun showStartLoad()

        /**
         * show youtube item loaded from api
         * @itemList arrayList item Youtube
         */
        fun showYoutubeItems(itemList: ArrayList<YouTubeItem>)

        /**
         * show detail item by navigate to detail activity
         * @itemData: youtube Item
         * @viewItem: current itemView
         */
        fun showDetailItems(itemData: YouTubeItem, viewItem: android.view.View)

        /**
         * show done load and close progress loading
         */
        fun showDoneLoad()
    }

    interface Presenter : BasePresenter {
        /**
         * refresh data
         */
        fun refreshData()

        /**
         * open data detail from item clicked
         * @youtubeItem : data item clicked
         * @view : view item clicked
         */
        fun openYoutubeDetail(youtubeItem: YouTubeItem, view: android.view.View)
    }
}