package m.empire.youtubedemo.main.view

import android.app.ActivityOptions
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.transition.Explode
import android.transition.Transition
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.gson.Gson
import m.empire.youtubedemo.R
import m.empire.youtubedemo.common.mvp.AdapterRecycleBase
import m.empire.youtubedemo.databinding.ActivityMainBinding
import m.empire.youtubedemo.detail.view.DetailYoutubeActivity
import m.empire.youtubedemo.main.MainActivityContract
import m.empire.youtubedemo.main.MainActivityPresenter
import m.empire.youtubedemo.main.YoutubeAdapter
import m.empire.youtubedemo.model.data.YouTubeItem

class MainActivity : AppCompatActivity(), MainActivityContract.View {


    companion object {
        val TAG = "MainActivity"
    }

    lateinit var binding: ActivityMainBinding
    lateinit var presenter: MainActivityContract.Presenter
    var youtubeAdapter = YoutubeAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(binding.toolbar)
        presenter = MainActivityPresenter(this, this)
        createView()
    }

    private fun createView() {
        binding.contentMain.swipeRefresh.setOnRefreshListener { presenter.refreshData() }
        var linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.contentMain.recycleViewYoutube.layoutManager = linearLayoutManager
        binding.contentMain.recycleViewYoutube.adapter = youtubeAdapter
        youtubeAdapter.itemClickCallBack = object : AdapterRecycleBase.ItemClickCallBack<YouTubeItem> {
            override fun onItemClick(position: Int, t: YouTubeItem, viewItem: View) {
                presenter.openYoutubeDetail(t, viewItem)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun showDetailItems(itemData: YouTubeItem, viewItem: View) {
        var intent = Intent(this, DetailYoutubeActivity::class.java)
        intent.putExtra(DetailYoutubeActivity.KEY_DATA, Gson().toJson(itemData))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var explode = Explode()
            window.allowEnterTransitionOverlap = false
            window.allowReturnTransitionOverlap = false
            explode.excludeTarget(android.R.id.statusBarBackground, true)
            explode.excludeTarget(android.R.id.navigationBarBackground, true)
            window.exitTransition = explode
            //
            var option = ActivityOptions.makeSceneTransitionAnimation(this, viewItem, "target")
            startActivity(intent, option.toBundle())
        } else {
            startActivity(intent)
        }
    }

    override fun showStartLoad() {
        binding.contentMain.swipeRefresh.isRefreshing = true
    }

    override fun showYoutubeItems(itemList: ArrayList<YouTubeItem>) {
        youtubeAdapter.addListData(itemList)
    }

    override fun showDoneLoad() {
        binding.contentMain.swipeRefresh.isRefreshing = false
        Log.e(TAG, "showDoneLoad")
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
