package m.empire.youtubedemo.main

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import m.empire.youtubedemo.R
import m.empire.youtubedemo.common.mvp.AdapterRecycleBase
import m.empire.youtubedemo.databinding.ItemYoutubeBinding
import m.empire.youtubedemo.model.data.YouTubeItem

/**
 * Created by PC on 10/6/2017.
 */
class YoutubeAdapter : AdapterRecycleBase<YouTubeItem, YoutubeAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent?.context).inflate(R.layout.item_youtube, parent, false)
        var viewHolder = ViewHolder(view)
        viewHolder.itemView.setOnClickListener({
            var position = viewHolder.adapterPosition
            itemClickCallBack?.onItemClick(position, arrayListData[position], viewHolder.itemView)
        })
        return viewHolder
    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = arrayListData[position]
        holder.binding.model = arrayListData[position]
        Glide.with(holder.itemView.context)
                .load(item.snippet.thumbnails.high.url)
                .into(holder.binding.itemYoutubeImages)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: ItemYoutubeBinding = DataBindingUtil.bind(itemView)

    }
}