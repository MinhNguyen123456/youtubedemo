package m.empire.youtubedemo.main

import android.app.Activity
import android.util.Log
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import m.empire.youtubedemo.model.data.YouTubeItem
import m.empire.youtubedemo.model.data.YouTubeSearchModel
import m.empire.youtubedemo.model.remote.YoutubeRemote
import m.empire.youtubedemo.util.ResponseHandler

/**
 * Created by PC on 10/6/2017.
 */
class MainActivityPresenter(private var activity: Activity, var view: MainActivityContract.View) : MainActivityContract.Presenter {


    var composite = CompositeDisposable()

    override fun start() {
        loadData()
    }

    override fun refreshData() {
        loadData()
    }

    override fun openYoutubeDetail(youtubeItem: YouTubeItem, view:View) {
        this.view.showDetailItems(youtubeItem, view)
    }

    private fun loadData() {
        var repository = YoutubeRemote(activity.applicationContext)
        composite.add(repository.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap { response -> ResponseHandler<YouTubeSearchModel>().mapResponse(response) }
                .subscribe(
                        {
                            Log.e("response", "data $it")
                            view.showYoutubeItems(it.youtubeItems)
                        },
                        {
                            view.showDoneLoad()
                            it.printStackTrace()
                            Log.e("error", it.message)
                        },
                        { view.showDoneLoad() }
                ))
    }


    override fun stop() {
        composite.clear()
    }


}