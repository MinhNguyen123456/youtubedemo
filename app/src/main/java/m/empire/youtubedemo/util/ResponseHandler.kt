package m.empire.youtubedemo.util

import android.util.Log
import io.reactivex.Observable
import retrofit2.Response
import java.lang.Exception

/**
 * Created by PC on 10/6/2017.
 */
class ResponseHandler<T> {
    fun mapResponse(response: Response<T>): Observable<T> {
        return if (response.isSuccessful) {
            Observable.just(response.body())
        } else {
            var messageError = response.errorBody()?.string()
            Log.e("error", "messageError : $messageError")
            Observable.error(Exception(messageError))
        }
    }
}